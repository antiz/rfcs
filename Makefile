FILES=*.md rfcs

all: check build

devel-watchexec:
	watchexec -e .scss -e .html -e .toml -e .rst -e .svg -e .png -e .md -e .txt -e .ini make

devel-http-server:
	python -m http.server -d public

build:
	hugo

check: lint

lint: lint-markdownlint lint-typos lint-vale

lint-markdownlint:
	mdl $(FILES)

lint-typos:
	typos $(FILES)

lint-vale:
	vale $(FILES)

clean:
	rm -rf public resources
