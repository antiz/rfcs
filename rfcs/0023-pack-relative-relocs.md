---
title: 0023 Add -Wl,-z,pack-relative-relocs
---

# Add -Wl,-z,pack-relative-relocs

- Date proposed: 2023-09-03
- RFC MR: <https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0023>

## Summary

Add `-Wl,-z,pack-relative-relocs` to our LDFLAGS in order to reduce the size of relocations.

## Motivation

`-z pack-relative-relocs` moves relative relocations from the `.rela.dyn` section into a new `.relr.dyn` section with a significantly more compact encoding, supported since `glibc` 2.36, GNU `Binutils` 2.38 and LLVM 15.

This can reduce the size of libraries a lot, e.g.  the installed size of `libphonenumber` dropped from about 17 MB to 7 MB.

`glibc` already enabled this for all its shared objects.
This can be confirmed by using `readelf -d` on an object and looking for the `RELR` tag.

For more info, see [FS#72433](https://bugs.archlinux.org/task/72433), the related blog post from [`MaskRay` on RELR](https://maskray.me/blog/2021-10-31-relative-relocations-and-relr) and the blog post from [`Glandium` on RELR](https://glandium.org/blog/?p=4297) for Firefox.

## Specification

Change our distributed LDFLAGS to the following:

```text
LDFLAGS="-Wl,-O1 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now \
         -Wl,-z,pack-relative-relocs"
```

We now also use one `-Wl` for each logical flag so that we can easily split at whitespace.
This does not change the meaning of the flags.

## Drawbacks

This will break compatibility with tools that process relocations and that have not been updated.
For example, `eu-readelf -r` does not decode the `.relr.dyn` section and `eu-elflint` reports an error.

We may encounter other regressions from this, but it seems unlikely that they are major.
Since `glibc` enabled this internally we should have encountered such already.

## Unresolved Questions

None.

## Alternatives Considered

None.
